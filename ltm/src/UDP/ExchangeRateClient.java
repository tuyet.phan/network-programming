package UDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ExchangeRateClient {
	
    public final static String SERVER_IP = "localhost";
    public final static int SERVER_PORT = 5000; // Cổng mặc định của Echo Server
    public final static byte[] BUFFER = new byte[1000]; // Vùng đệm chứa dữ liệu cho gói tin nhận

	public ExchangeRateClient() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			DatagramSocket soc = new DatagramSocket();
			String str = "Exchange Yen to VND";
			InetAddress ip = InetAddress.getByName(SERVER_IP);			
			
			DatagramPacket outsending = new DatagramPacket(str.getBytes(), str.length(), ip, SERVER_PORT);			
			soc.send(outsending);
			
			DatagramPacket incoming = new DatagramPacket(BUFFER, BUFFER.length);
			soc.receive(incoming);
			
			int l = incoming.getLength();
			String message = new String(incoming.getData()).substring(0, l);
			
			System.out.println(message);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
