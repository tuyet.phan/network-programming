package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Server {
	
	 public final static int SERVER_PORT = 3000; // Cổng mặc định của Echo Server
	 public final static byte[] BUFFER = new byte[1000]; // Vùng đệm chứa dữ liệu cho gói tin nhận
	 
	public static void main(String[] args) {
		DatagramSocket ds = null;
		try {
			System.out.println("Binding to port " + SERVER_PORT + ", please wait  ...");
			ds = new DatagramSocket(SERVER_PORT); // Tạo Socket với cổng là 7
            System.out.println("Server started ");
            System.out.println("Waiting for messages from Client ... ");
			
			while(true){
				//create packet incoming
				DatagramPacket incoming = new DatagramPacket(BUFFER, BUFFER.length);
				ds.receive(incoming);
				String message = new String(incoming.getData()).toUpperCase();
				 System.out.println("Received: " + message);
				
				//create packet outsendng
				DatagramPacket outsendng = new DatagramPacket(message.getBytes(),message.length(),incoming.getAddress(),incoming.getPort());
				ds.send(outsendng);
			}
		} catch (IOException e) {
            e.printStackTrace();
        } 
		finally {
			if(ds!=null) {
				ds.close();
			}
		}
	}

}
