package UDP;

import java.awt.List;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

public class ExchangeRateServer {
	
	public final static int SERVER_PORT = 5000; // Cổng mặc định của Echo Server
    public final static byte[] BUFFER = new byte[1000]; // Vùng đệm chứa dữ liệu cho gói tin nhận 
	static ArrayList<String> loaitien = new ArrayList<String>();
	static HashMap<String, String> hashMap = new HashMap<String, String>();
	public ExchangeRateServer() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		hashMap.put("Yen-VND","200");
	    hashMap.put("Yen-USD","0.01");
	    hashMap.put("VND-Yen","0.005");
	    hashMap.put("VND-USD","0.00004");
	    hashMap.put("USD-Yen","108");
	    hashMap.put("USD-VND","23200");
		try {
			 System.out.println("Binding to port " + SERVER_PORT + ", please wait  ...");
			 DatagramSocket soc = new DatagramSocket(SERVER_PORT);
            System.out.println("Server started ");
            System.out.println("Waiting for messages from Client ... ");           
						
			while(true) {
				
				//create incoming packet
				DatagramPacket incoming = new DatagramPacket(BUFFER, BUFFER.length);
				soc.receive(incoming);
				int l = incoming.getLength();
				String str = new String(incoming.getData()).substring(0, l);
				InetAddress ip = incoming.getAddress();
				int port = incoming.getPort();
				
				String loaitien = "(Yen|VND|USD)";
				String temp = "Exchange\\s" + loaitien + "\\s+to\\s+" + loaitien;
				String message = new String();
				if(Pattern.matches(temp, str)) {
					String loaitien1 = str.split(" ")[1];
					String loaitien2 = str.split(" ")[3];
					message = hashMap.get(loaitien1 + "-" +loaitien2);
				} else {
					message = "Wrong Systax";
				}				
				//create outsending packet
				DatagramPacket  outsending = new DatagramPacket(message.getBytes(), message.length(), ip, port);
				soc.send(outsending);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
