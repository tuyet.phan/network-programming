package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class Client {

    public final static String SERVER_IP = "127.0.0.1";
    public final static int SERVER_PORT = 3000; // Cổng mặc định của Echo Server
    public final static byte[] BUFFER = new byte[1000]; // Vùng đệm chứa dữ liệu cho gói tin nhận
	
	public static void main(String[] args) {	
		DatagramSocket ds = null;
		try {
			ds = new DatagramSocket();
			System.out.println("Client started ");
			
			
			InetAddress server = InetAddress.getByName(SERVER_IP);
			while(true) {
				Scanner scanner = new Scanner(System.in);
				//Enter the message want to send
				String message = scanner.nextLine();
				
				//outsending packet
				DatagramPacket outsending = new DatagramPacket(message.getBytes(), message.length(),server,SERVER_PORT);
				ds.send(outsending);
				
				// incoming packet
				DatagramPacket incoming = new DatagramPacket(BUFFER, BUFFER.length);
				ds.receive(incoming);
				
				// change byte data into string
				System.out.println(new String(incoming.getData()));
				scanner.close();
			}
		} catch (IOException e) {
            System.err.println(e);
        }
		finally {
            if (ds != null) {
                ds.close();
            }
	}
}
}
