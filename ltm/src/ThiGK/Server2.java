package ThiGK;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server2 {

	public static boolean checkSent= false;
    public final static int SERVER_PORT = 7100;
	ArrayList<Person> people = new ArrayList<Person>();
	
	public Server2() {
		try {
			people.add(new Person("Minh Tuyet Phan", 21));
			people.add(new Person("Nguyen Thi Lan", 24));
			people.add(new Person("Tuong", 90));
			people.add(new Person("Duong", 18));
			people.add(new Person("Phu", 22));
			people.add(new Person("Long", 44));
			
			System.out.println("Binding to port " + SERVER_PORT + ", please wait  ...");
			ServerSocket server = new  ServerSocket(7100);
			System.out.println("Server2 started: " + server);
			System.out.println("Waiting for a client ...");
			
			while (true) {
				Socket socket = server.accept();
				System.out.println("Connected");
				Mythread2 mythread = new Mythread2(socket, this);
				mythread.start();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public static void main(String[] args) {
		new Server2();
	}
}

class Mythread2 extends Thread{
	Socket soc ;
	Server2 server;
	boolean check=false;
	boolean sented = false;
	public Mythread2(Socket soc,Server2 server) {
		this.soc = soc;
		this.server = server;
	}
	public String findAge(String s) throws IOException {
	for(Person p : server.people)
	{
		if(p.name.equals(s)) return p.age+"";
	}
	check=true;
	Socket socket = new Socket("localhost",7000);

	DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
	dos.writeUTF(s);

	DataInputStream dis1 = new DataInputStream(socket.getInputStream());
	return dis1.readUTF();
	}
	@Override
	public void run() {
		try {
			DataInputStream dis = new DataInputStream(soc.getInputStream());
			String s= dis.readUTF();
			System.out.println(s);
			DataOutputStream dos3 = new DataOutputStream(soc.getOutputStream());
			dos3.writeUTF(findAge(s)+"");
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.run();
	}
}