package ThiGK;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
//import java.util.Scanner;

public class Server1 {

	ArrayList<Person> people = new ArrayList<Person>();
    public final static int SERVER_PORT = 7000;

	public Server1() {
		try {
			people.add(new Person("Anh 2", 10));
			people.add(new Person("Khoa ", 14));
			people.add(new Person("Nhung ", 16));
			people.add(new Person("KHOa ", 18));
			people.add(new Person("Lan ", 22));
			people.add(new Person("HUe ", 44));
			
			System.out.println("Binding to port " + SERVER_PORT + ", please wait  ...");
			ServerSocket server = new  ServerSocket(SERVER_PORT);
			System.out.println("Server1 started: " + server);
			System.out.println("Waiting for a client ...");
			
			while (true) {
				Socket socket = server.accept();
				System.out.println("Connected");
				Mythread1 mythread = new Mythread1(socket, this);
				mythread.start();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}


	}
	public static void main(String[] args) {
		new Server1();
	}
}

class Mythread1 extends Thread{
	Socket soc ;
	Server1 server1;
	public Mythread1(Socket soc,Server1 server) {
		this.soc = soc;
		this.server1 = server;
	}
	@Override
	public void run() {
		try {
			DataInputStream dis = new DataInputStream(soc.getInputStream());
			String s= dis.readUTF();
			System.out.println(s);
			DataOutputStream dos3 = new DataOutputStream(soc.getOutputStream());
			dos3.writeUTF( findAge(s)+"");
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.run();
	}
	private String findAge(String s) throws IOException {

		for(Person p : server1.people)
		{
			if(p.name.equals(s) ) return p.age+" ";
		}
		Socket socket = new Socket("localhost",7100);
		DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
		dos.writeUTF(s);

		DataInputStream dis1 = new DataInputStream(socket.getInputStream());
		return dis1.readUTF();
	}
}