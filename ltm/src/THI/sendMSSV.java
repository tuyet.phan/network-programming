package THI;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class sendMSSV {
	
	public final static String SERVER_IP = "192.168.10.128";
    public final static int SERVER_PORT = 54182;
    
	public static void main(String[] args) throws IOException {
				
		Socket soc = null;
		DataOutputStream dos = null;
		DataInputStream dis = null;
		try {
			soc = new Socket(SERVER_IP,SERVER_PORT);
			System.out.println("Connected: " + soc);
			
			dos = new DataOutputStream(soc.getOutputStream());
			dis = new DataInputStream(soc.getInputStream());
			Scanner sc = new Scanner(System.in);
			while(true) {
				dos.writeUTF("Send MSSV,102160204");		
//				str = dis.readUTF(); // receive data
				String strUpper = dis.readUTF().toUpperCase();
				dos.writeUTF(strUpper);
				System.out.println("Uppercase String: " + strUpper);	
			}
		}
		catch(IOException e) {
			System.out.println("Disconnect");
		}
		finally {
			if (soc != null)
				soc.close();
		}
	}

}

