package gk;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class client {

	public final static String SERVER_IP = "localhost";
    public final static int SERVER_PORT = 7000;
    
	public static void main(String[] args) throws IOException {
				
		Socket soc = null;
		DataOutputStream dos = null;
		DataInputStream dis = null;
		try {
			soc = new Socket(SERVER_IP,SERVER_PORT);
			System.out.println("Connected: " + soc);
			
			dos = new DataOutputStream(soc.getOutputStream());
			dis = new DataInputStream(soc.getInputStream());
			
			while(true) {				
				dos.writeUTF("25");
				System.out.println("Result : " + dis.readUTF());		
			}
		}
		catch(IOException e) {
			System.out.println("Disconnect");
		}
		finally {
			if (soc != null)
				soc.close();
		}
	}

}

