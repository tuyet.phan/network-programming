package gk;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;

public class server {
	
	public final static int SERVER_PORT = 5000;
	private static ServerSocket serverSocket = null;
	
	public static void main(String[] args) throws IOException{
		
		DataOutputStream dos = null;
		DataInputStream dis = null;	
		
		try {
				ArrayList<Student> listStudent = new ArrayList<Student>();				
				Student student1 = new Student("Minh Tuyet Phan", 21);
		        Student student2 = new Student("Phan Thi Ba", 22);
		        Student student3 = new Student("Vo Van Sy", 21);
		        Student student4 = new Student("Ly Thi Dieu", 20);
		        Student student5 = new Student("Ho Thi Lan", 21);
		        
		        // Add objects to listStudent
		        listStudent.add(student1);
		        listStudent.add(student2);
		        listStudent.add(student3);
		        listStudent.add(student4);
		        listStudent.add(student5);

		        // Show listStudent
		        String str = "";
		        String str1 = "";
		        
		        for (Student student : listStudent) {
		        	str = student.getName().toString() + student.getAge();
		        	str1 = str1.concat(str);
		        	
		        	
		        }		      					        
				System.out.println("Binding to port " + SERVER_PORT + ", please wait  ...");
				serverSocket = new ServerSocket(SERVER_PORT);
				System.out.println("Server started: " + serverSocket);
				System.out.println("Waiting for a client ...");
				  
			  Socket clientSocket = null;
			  clientSocket = serverSocket.accept();
			  System.out.println("Client accepted: " + clientSocket.getInetAddress()); 
			  
			  dos =  new DataOutputStream(clientSocket.getOutputStream());
			  dis = new DataInputStream(clientSocket.getInputStream());
			  
			while(true) {
				if(dis.readUTF().compareTo("Getall")==0) {
					dos.writeUTF(str1);
				}
				else 
				{
					dos.writeUTF("Nhap chuoi Getall de lay thong tin");
					
				}
			}		  
		}
		catch(Exception e) 
		{
	       dos.close();
	       serverSocket.close();
	       dis.close();
	       System.out.print(e.getMessage());
	    }
	}
}

class Student {
	
    private String name;
    private int age;
    public Student(String name, int age) {
        super();
        this.name = name;
        this.age = age;
    }
    public String getName() {
        return name;
    }
    public int getAge() {
        return age;
    }
    public void setName(String name) {
        this.name = name;
    }    
    public void setAge(int age) {
        this.age = age;
    }    
    @Override
    public String toString() {
        return "Student@[name=" + name + ", age=" + age + "]";
    }
}