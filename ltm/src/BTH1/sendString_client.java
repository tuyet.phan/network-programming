package BTH1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class sendString_client {
	
	public final static String SERVER_IP = "localhost";
    public final static int SERVER_PORT = 5000;
    
	public static void main(String[] args) throws IOException {
				
		Socket soc = null;
		DataOutputStream dos = null;
		DataInputStream dis = null;
		try {
			soc = new Socket(SERVER_IP,SERVER_PORT);
			System.out.println("Connected: " + soc);
			
			dos = new DataOutputStream(soc.getOutputStream());
			dis = new DataInputStream(soc.getInputStream());
			Scanner sc = new Scanner(System.in);
			String str = "";
			while(true) {
				System.out.println("Original string: ");
				str = sc.nextLine();
				dos.writeUTF(str);				
				String strUpper = dis.readUTF();
				String strLower = dis.readUTF();
				String strCount = dis.readUTF();
				System.out.println("Uppercase String: " + strUpper);				
				System.out.println("Lowercase String: " + strLower);
				System.out.println("The number of words in the string:" + strCount);
			}
		}
		catch(IOException e) {
			System.out.println("Disconnect");
		}
		finally {
			if (soc != null)
				soc.close();
		}
	}

}
