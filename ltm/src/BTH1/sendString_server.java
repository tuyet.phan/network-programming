package BTH1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class sendString_server {
	
	public final static int SERVER_PORT = 5000;
	
	public static void main(String[] args) throws IOException {
		
		ServerSocket serverSocket = null;		
		try 
		{
			System.out.println("Binding to port " + SERVER_PORT + ", please wait  ...");
            serverSocket = new ServerSocket(SERVER_PORT);
            System.out.println("Server started: " + serverSocket);
            System.out.println("Waiting for a client ...");		
            
			while(true) {
				try {
                    Socket socket = serverSocket.accept();
                    System.out.println("Client accepted: " + socket); 
                    DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
                    DataInputStream dis = new DataInputStream(socket.getInputStream());
        			String str = "";
                while(true){
						str = dis.readUTF(); // receive data
					    String strUpper = str.toUpperCase();
					    String strLower = str.toLowerCase();
					    String strcount = Integer.toString(str.length());					    
			            dos.writeUTF(strUpper); // send data
			            dos.writeUTF(strLower);			            
			            dos.writeUTF(strcount);
		            }
				}
				catch(IOException e){
					System.err.print("Connection Error!" + e);
				}
			}
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if (serverSocket != null)
				serverSocket.close();
		}
	}

}
