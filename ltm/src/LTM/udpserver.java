package LTM;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class udpserver {

	public static void main(String[] args) throws Exception {
		
		DatagramSocket serverSocket = new DatagramSocket(8876);
		byte[] receiveData= new byte[1024];
		byte[] sentData= new byte[1024];
		while(true) {
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			serverSocket.receive(receivePacket);
			String sentence = new String(receivePacket.getData());
			InetAddress IPAdress = receivePacket.getAddress();
			int Port = receivePacket.getPort();
			System.out.println(IPAdress+" , "+Port+" , "+sentence);
			
			String capsent= sentence.toUpperCase();
			sentData= capsent.getBytes();
			DatagramPacket sentPacket = new DatagramPacket(sentData, sentData.length,IPAdress,Port);
			serverSocket.send(sentPacket);
			
			
		}

	}

}