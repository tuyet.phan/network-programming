package LTM;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;


//Xoa Repaint, -> CaroClient
public class GameCaro extends JFrame implements MouseListener
{
	int s = 15;
	int so = 15;
	int of = 50;
	List<Point> dadanh = new ArrayList();
	public static void main(String[] args) {
		new GameCaro();
		// TODO Auto-generated method stub
	}
	
	public GameCaro()
	{
		this.setTitle("Co Caro");
		this.setSize(s*so+2*of, s*so+2*of);
		this.setDefaultCloseOperation(3);
		this.addMouseListener(this);
		this.setVisible(true);
	}
	
	public void paint(Graphics g)
	{
		g.setColor(Color.WHITE);
		g.fillRect(0,0,this.getWidth(), this.getHeight());
		g.setColor(Color.BLACK);
		for (int i=0; i <= s; i++)
		{
			g.drawLine(of, of+i*s, of+so*s,of+i*s);
			g.drawLine(of+i*s,of,of+i*s,of+so*s);
		}
		
		g.setFont(new Font("arial", Font.BOLD,s));
		for (int i =0; i<dadanh.size();i++)
		{
			String st = "o";
			g.setColor(Color.blue);
			if(i%2==0)
			{
				st = "x";
				g.setColor(Color.red);				
			}
			
			int x = dadanh.get(i).x*s+of+s-s/2-s/4-s/8-s/10;
			int y = dadanh.get(i).y*s+of+s-s/4;
			g.drawString(st,x,y);
			
		}
		
		
	}
	
	@Override
	public void mouseClicked(MouseEvent e)
	{
		int x = e.getX();
		int y = e.getY();
		if(x<of || x >= of+so*s) return;
		if(y<of || y >= of+so*s) return;
		
		int idx = (x-of)/s;
		int idy = (y-of)/s;
		
		for(Point p:dadanh)
		{
			if(idx==p.x && idy==p.y) return;
			
		}
		dadanh.add(new Point(idx,idy));
		System.out.print(idx + "," + idy);
	

		this.repaint();

	}
	
	

	

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
