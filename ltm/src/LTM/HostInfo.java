package LTM;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class HostInfo {

	public static void main(String[] args) {
		try {
			InetAddress myhost = InetAddress.getLocalHost();
			System.out.println(myhost.getHostAddress());
			System.out.print(myhost.getHostName());
		}
		catch (UnknownHostException ex) {
			System.err.print("Cannot find local host");
		}

	}

}