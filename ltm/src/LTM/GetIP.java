package LTM;

import java.net.Socket;
import java.io.DataInputStream;
import java.util.Scanner;

public class GetIP {

	public final static int SERVER_PORT = 8888;
	
	public static void main(String[] args) {
		try {
			Scanner sc = new Scanner (System.in);			
			Socket mysoc = new Socket(sc.next(), SERVER_PORT);
			System.out.println("Connected: " + mysoc);		
			
			DataInputStream dis = new DataInputStream(mysoc.getInputStream());
			
			System.out.print(dis.readUTF());
			sc.close();
			mysoc.close();
		}
		catch (Exception ex) {
			System.err.print("Cannot find local host");
		}
		
	}
}