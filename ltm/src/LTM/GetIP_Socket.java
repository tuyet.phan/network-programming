package LTM;

import java.net.Socket;
import java.util.Scanner;
public class GetIP_Socket {

	public final static int SERVER_PORT = 8888;

	public static void main(String[] args) {
		try {
			Scanner sc = new Scanner (System.in);
			Socket mysoc = new Socket(sc.next(), SERVER_PORT);
			System.out.println("Connected: " + mysoc);				
			System.out.println("Server Addres: " + mysoc.getInetAddress());
			System.out.println("Server Port: " + mysoc.getPort());
			System.out.println("Local Address: " + mysoc.getLocalAddress());
			System.out.println("Local Port: " + mysoc.getLocalPort());
			sc.close();		
			mysoc.close();
		}
		catch (Exception ex) {
			System.err.print("Cannot find local host");
		}
	}
}