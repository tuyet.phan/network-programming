package LTM;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
//Tiet sau:
// 1. Trien khai tren 2 may
// 2. Them chuc nang nhieu nguoi choi
// Bang cach: Tao ban, tham gia ban

//Xoa Repaint, -> CaroClient
//Them list/vector quan li ban
public class CaroClient extends JFrame implements MouseListener,Runnable
{
	int s = 15;
	int so = 15;
	int of = 50;
	List<Point> dadanh = new ArrayList();
	Socket soc;
	public static void main(String[] args) {
		new CaroClient();
		// TODO Auto-generated method stub
	}
	
	public CaroClient()
	{
		this.setTitle("Co Caro");
		this.setSize(s*so+2*of, s*so+2*of);
		this.setDefaultCloseOperation(3);
		try {
			this.soc = new Socket("127.20.10.1", 5000);
			
		}
		catch(Exception e)
		{
		
		}
		this.addMouseListener(this);
		this.setVisible(true);
		Thread t = new Thread(this);
		t.start();
	}
	
	public void paint(Graphics g)
	{
		g.setColor(Color.WHITE);
		g.fillRect(0,0,this.getWidth(), this.getHeight());
		g.setColor(Color.BLACK);
		for (int i=0; i <= s; i++)
		{
			g.drawLine(of, of+i*s, of+so*s,of+i*s);
			g.drawLine(of+i*s,of,of+i*s,of+so*s);
		}
		
		g.setFont(new Font("arial", Font.BOLD,s));
		for (int i =0; i<dadanh.size();i++)
		{
			String st = "o";
			g.setColor(Color.blue);
			if(i%2==0)
			{
				st = "x";
				g.setColor(Color.red);				
			}
			
			int x = dadanh.get(i).x*s+of+s-s/2-s/4-s/8-s/10;
			int y = dadanh.get(i).y*s+of+s-s/4;
			g.drawString(st,x,y);
			
		}
		
		
	}
	
	@Override
	public void run()
	{
		while(true)
		{
			try {
				DataInputStream dis = new DataInputStream(soc.getInputStream());
				int idx = Integer.parseInt(dis.readUTF());
				int idy = Integer.parseInt(dis.readUTF());
				this.dadanh.add(new Point(idx, idy));
				this.repaint();
			}
			catch(Exception e)
			{
				
			}
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e)
	{
		int x = e.getX();
		int y = e.getY();
		if(x<of || x >= of+so*s) return;
		if(y<of || y >= of+so*s) return;
		
		int idx = (x-of)/s;
		int idy = (y-of)/s;
		
		try {
			DataOutputStream dos = new DataOutputStream(soc.getOutputStream());
			dos.writeUTF(idx+"");
			dos.writeUTF(idy+"");
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
	

		this.repaint();

	}	

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub		
	}
	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub		
	}


}
