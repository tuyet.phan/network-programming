package LTM;

import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class DatetimeServer {
	
	public final static int SERVER_PORT = 5000;
	
	public static void main(String[] args) {
		
		try {
			ServerSocket server = new ServerSocket(SERVER_PORT);
			
			while(true)
			{
				Socket soc  = server.accept();
				DataOutputStream dos = new DataOutputStream(soc.getOutputStream());
				String str = new Date().toString();
				dos.writeUTF("Tuyet M. Phan Server" + str);
				soc.close();
			}
		}
		catch( Exception e)
		{
			System.out.print("Error!");
		}
	}
}