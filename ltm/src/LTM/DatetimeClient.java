package LTM;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class DatetimeClient {
	
	public final static int SERVER_PORT = 5000;
	
	public static void main(String[] args) {
		
		try {
			ServerSocket server = new ServerSocket(SERVER_PORT);
			while(true)
			{
				Socket soc = server.accept();
				MyThread mt= new MyThread(soc);
				mt.run();
			}
		}
		catch( Exception e)
		{
			System.out.print("Error!");
		}
	}
}
class MyThread extends Thread{
	Socket soc;
	MyThread(Socket soc)
	{
		this.soc = soc;
	}
	public void run()
	{
		DataOutputStream dos;
		try {
			System.out.print("Connection"+ this.soc.getInetAddress());
			dos = new DataOutputStream(this.soc.getOutputStream());
			String str = new Date().toString();
			dos.writeUTF("Phan Minh Tuyet server" + str);
			this.soc.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}