package LTM;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;
//import javax.swing.JFrame;

public class udpclient {

	public static void main(String[] args) throws Exception{
		Scanner input=new Scanner(System.in);
		while(true) {
		DatagramSocket clientSocket= new DatagramSocket();
		InetAddress IPAddress= InetAddress.getByName("Localhost");		
		byte[] sendData =new byte[1024];
		byte[] receiveData = new byte[1024];		
		String sentence =input.nextLine();
		sendData = sentence.getBytes();
		DatagramPacket sendPacket =new DatagramPacket(sendData, sendData.length,IPAddress,8876);
		clientSocket.send(sendPacket);
		DatagramPacket receivePacket = new DatagramPacket(receiveData, sentence.length());
		clientSocket.receive(receivePacket);
		String ModifiedSentence = new String(receivePacket.getData());
		System.out.println(ModifiedSentence);
		clientSocket.close();
		}
	}

}